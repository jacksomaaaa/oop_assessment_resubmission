/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_assignment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class AddUserFunction {

    public void adddocuser(Doctor new_doctor) throws IOException {
        Gson gson = new GsonBuilder().create();
        Reader reader = Files.newBufferedReader(Paths.get("dac.json"));
        java.lang.reflect.Type listType = new TypeToken<ArrayList<Doctor>>() {}.getType();
        ArrayList<Doctor> sArray = gson.fromJson(reader, listType);  
        sArray.add(new_doctor);
        try ( Writer writer = new FileWriter("dac.json")) {
            gson.toJson(sArray, writer);
        } catch (Exception e) {
            System.out.println("error meassage!");
        }
    }
     public void addpuser(Patient new_patient) throws IOException {

        Gson gson = new GsonBuilder().create();
        Reader reader = Files.newBufferedReader(Paths.get("pac.json"));
        java.lang.reflect.Type listType = new TypeToken<ArrayList<Patient>>() {
        }.getType();
        ArrayList<Patient> sArray = gson.fromJson(reader, listType);
        
        sArray.add(new_patient);
        try ( Writer writer = new FileWriter("pac.json")) {
            gson.toJson(sArray, writer);
        } catch (Exception e) {
            System.out.println("error meassage!");
        }
    }
      public void addsuser(Secretary new_secretary) throws IOException {

        Gson gson = new GsonBuilder().create();
        Reader reader = Files.newBufferedReader(Paths.get("sac.json"));
        java.lang.reflect.Type listType = new TypeToken<ArrayList<Patient>>() {
        }.getType();
        ArrayList<Secretary> sArray = gson.fromJson(reader, listType);
        
        sArray.add(new_secretary);
        try ( Writer writer = new FileWriter("sac.json")) {
            gson.toJson(sArray, writer);
        } catch (Exception e) {
            System.out.println("error meassage!");
        }
    }
}
