/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_assignment;

/**
 *
 * @author tss
 */
public class Evaluation {
    private String doctorname;
    private String performance;
        public Evaluation ()
        {
        doctorname = "";
        performance = "";
}
        public Evaluation (String doctorname, String performance)
        {
        this.doctorname = doctorname;
        this.performance = performance;
}
        public String getdoctorname ()
        {
            return doctorname;
        }
         public String getperformance ()
        {
            return performance;
        }
         public void setdoctorname (String doctorname)
         {
             this.doctorname = doctorname;
         }
         
         public void setperformance (String performance)
         {
             this.performance = performance;
         }
         
         public String toString ()
         {
             return doctorname + performance;
         }
             
}
