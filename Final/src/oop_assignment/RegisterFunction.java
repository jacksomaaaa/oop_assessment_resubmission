/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_assignment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class RegisterFunction {

    public void registeruser(RegisterPatient new_register) throws IOException {

        Gson gson = new GsonBuilder().create();
        Reader reader = Files.newBufferedReader(Paths.get("rac.json"));
        java.lang.reflect.Type listType = new TypeToken<ArrayList<RegisterPatient>>() {
        }.getType();
        ArrayList<RegisterPatient> sArray = gson.fromJson(reader, listType);   
        
        sArray.add(new_register);
        try ( Writer writer = new FileWriter("rac.json")) {
            gson.toJson(sArray, writer);
        } catch (Exception e) {
            System.out.println("error meassage!");
        }
    }
  
}
